This is Jon Jenne's Cambridge Mobile Telematics Weather project. My email is jon@jonter.net

I used the following depencies:

	* ActiveAndroid for easy database modeling and storage
	* GSON for easy API processing
	* Retrofit 2 for easy API retrieval
	* Smart location library for simpler location access


The app should theoretically work on any real device starting at SDK 16. However, I don't have a device this low to test on, and the emulator fails because Google Play Services version is too low. Due to it being an emulator, there is no Google Play Store to update the Google Play Services, so location services cannot be used. Any device with Google Play Services at a modern version should work. 

I've tested this on the following devices:

	Emulators:
	* Android SDK 28 (Works fine)
	* Android SDK 24 (Works fine, if GPS is updated)
	* Android SDK 17 (No Google Play Store, so Google Play Services cannot be installed/updated)
	
	Real Devices:
	* Essential PH-1 SDK 28 (Works fine)
	* Nexus 5X SDK 27 (Works fine)

Basically, when the App is launched for the first time, it will immediately check for location permission. Since that is required by Android to be given explicitly, everything comes to a screeching halt until we get that. Once it's granted, the app can move on to attempting to determine the device's last known location. I didn't want to wait for a GPS lock, because that can take a long time and typically a last known location is "good enough". (I don't foresee you testing my app after flying from Boston to Shanghai, so I think any last location on your devices might be close enough.) Once the location permission is granted, and a location is found, it will reach out to the darksky api with a Lat/Long and reterive the forecast data. It then stores this into a Database (using ActiveAndroid POJO Models). Once the saving is done, it updates the UI that is already being shown to the latest data that has just been retrieved, so no new Activity or Layout is required. If you would like to change the location, you can enter a zip code on the top of the screen, and the device will automatically clear the previous data and retrieve data based on this zip code. Whenever the app is opened fresh, it will default to it's last known location, sometimes overriding the previously entered zip code. See "If I had more time" below. 

The UI isn't so spectacular, just the bare minimum to accomplish the goal. The MainActivity has 0 buttons. The app should automatically gather your location and fetch the weather for you. The app will attempt to re-check for fresh data every time onResume() is called. 

I've tried to test manually for failure conditions, like no internet, no data, etc. I did not write any formal unit testing, since I'm a bit unfamiliar with that. The app should be smart enough to gracefully handle most error conditions you throw it's way. It should also not ask for any permisisons that have not been explicitly stated in the manifest, with the exception of a UI prompt for Location on first run.

If I had more time, I would: 

	* Put the Zipcode to Location lookup in a background thread. Right now it's on the UI and it can cause a little bit of a stutter. 
	* Have the Zipcode data get saved in SharedPreferences or DB so that onResume correctly calls the Zipcode location instead of the actual device location. Or, just clear the ZipCode so the users knows to re-enter it.
	* Allow saving multiple locations so that as long as the data was current you could quickly (and without network) retreive the stored data. 
	* Fix a bug where if you close the app and re-open it after having changed the town manually with a zipcode, the app will dutifuly attempt to retrieve new data for the device's current location. However, if the network is cut out, this will fail, although the device still shows the current town location. 

If you have any questions or any difficulty running the app, let me know at : jon@jonter.net and I'll be happy to help. 

