package net.jonter.cambridgemobiletelematicsweatherproject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

import net.jonter.cambridgemobiletelematicsweatherproject.models.Forecast;

import java.util.List;

public interface WeatherApi
{
    @GET("forecast/d42be27722537fa579e66d03b58cbfca/{latlong}")
    Call<Forecast> getForecast(@Path(value = "latlong", encoded = true) String latlong);

}
