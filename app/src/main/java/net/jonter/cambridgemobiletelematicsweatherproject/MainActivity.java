package net.jonter.cambridgemobiletelematicsweatherproject;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.GsonBuilder;

import net.jonter.cambridgemobiletelematicsweatherproject.models.Currently;
import net.jonter.cambridgemobiletelematicsweatherproject.models.Daily;
import net.jonter.cambridgemobiletelematicsweatherproject.models.DailyData;
import net.jonter.cambridgemobiletelematicsweatherproject.models.Forecast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import ly.count.android.sdk.Countly;
import ly.count.android.sdk.DeviceId;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private int LOCATION_PERMISSION = 123;

    private FusedLocationProviderClient mFusedLocationClient;
    private TextView townName;
    private TextView insertTown;

    private TextView todayTemp;
    private TextView todayHumidity;
    private TextView todaySummary;

    private int FORECAST_LIMIT = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableCrashlytics();

        setContentView(R.layout.activity_main);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        townName = findViewById(R.id.townName);
        insertTown = findViewById(R.id.insertTown);
        todayTemp = findViewById(R.id.today_temp_data);
        todayHumidity = findViewById(R.id.today_humidity_data);
        todaySummary = findViewById(R.id.today_summary_data);
        ActiveAndroid.initialize(this);

        insertTown.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                //basic check if it's a 5-digit post code or not
                if (s.length() == 5) {
                    Address address = getLocationFromZip(s.toString());
                    setTownName(address);
                    getForecastData(address);
                }
            }
        });
    }

    private void getForecastData(Address address)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.darksky.net/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                .build();

        WeatherApi apiService = retrofit.create(WeatherApi.class);

        Call<Forecast> callForecasts = apiService.getForecast(address.getLatitude()+","+address.getLongitude());
        callForecasts.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response)
            {
                ActiveAndroid.beginTransaction();
                new Delete().from(Forecast.class).where("").execute(); //clear out the local DB before adding new
                new Delete().from(Currently.class).where("").execute();
                new Delete().from(Daily.class).where("").execute();
                new Delete().from(DailyData.class).where("").execute();

                Forecast f = response.body();
                f.save();
                f.currently.save();
                for(DailyData day : f.daily.data)
                {
                    day.save();
                }

                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();

                setForecastUIElements();
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {
                // Log error here since request failed
                Log.e("THIS", t.toString());
            }
        });
    }
    @Override
    protected void onResume()
    {
        setUI();
        super.onResume();
    }
    /*
        Returns true if permission is granted already, and false if it needed to be prompted.
     */
    private boolean checkLocationPermission()
    {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION);
            return false;
        }
        else
        {
            return true;
        }
    }

    private void setUI()
    {
        setLocationUIElements();
        setForecastUIElements();
    }
    private void setTownName(Address address)
    {
        townName.setText(getString(R.string.forecast_for, address.getLocality(), address.getAdminArea()));
    }


    private void setLocationUIElements()
    {
        if (checkLocationPermission()) {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    try {
                        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                        setTownName(addresses.get(0));
                        getForecastData(addresses.get(0));
                        }
                    catch (Exception e) {
                    }
                }
            });
        }
    }
    private void setForecastUIElements()
    {
        Forecast forecast = getForecast();
        if(forecast != null)
        {
        }
        Currently currently = getCurrently();
        if(currently != null)
        {
            todayTemp.setText(currently.temperature);
            todayHumidity.setText(currently.humidity);
            todaySummary.setText(currently.summary);
        }
        TableLayout dailyTable = (TableLayout) findViewById(R.id.daily_weather_table);
        /*
         * Removeallviews needs to be called here, because this method is actually called twice.
         * First, it's called to show data from the DB.
         * Second, it's called to show updated data from the API
         */
        dailyTable.removeAllViews();
        dailyTable.isStretchAllColumns();

        List<DailyData> daily = getDailyData();
        if(daily != null) {
            int index=0;
            for (DailyData day : daily)
            {
                if(index < FORECAST_LIMIT) {
                    dailyTable.addView(getDateRow(day));
                    dailyTable.addView(getAvgTempRow(day));
                    dailyTable.addView(getHumidityRow(day));
                    dailyTable.addView(getSummaryRow(day));
                }
                index++;

            }
        }
    }
    private TableRow getSummaryRow(DailyData data)
    {
        TableRow tr = getNewTableRow();

        TextView summary = new TextView(MainActivity.this);
        summary.setText(data.summary);
        TableRow.LayoutParams params = (TableRow.LayoutParams) tr.getLayoutParams();
        params.span = 2; //amount of columns you will span
        summary.setLayoutParams(params);
        summary.setBackground(getResources().getDrawable(R.drawable.border));

        tr.addView(summary);
        return tr;
    }
    private TableRow getHumidityRow(DailyData data)
    {

        TextView temp = new TextView(MainActivity.this);
        temp.setText(getString(R.string.humidity));
        temp.setBackground(getResources().getDrawable(R.drawable.border));

        TextView tempData = new TextView(MainActivity.this);
        tempData.setText(data.humidity);
        tempData.setBackground(getResources().getDrawable(R.drawable.border));

        TableRow tr = getNewTableRow();

        tr.addView(temp);
        tr.addView(tempData);
        return tr;
    }
    private TableRow getAvgTempRow(DailyData data)
    {

        TextView temp = new TextView(MainActivity.this);
        temp.setText(getString(R.string.temperature));
        temp.setBackground(getResources().getDrawable(R.drawable.border));

        TextView tempData = new TextView(MainActivity.this);
        tempData.setText(data.getAverageTemperature());
        tempData.setBackground(getResources().getDrawable(R.drawable.border));

        TableRow tr = getNewTableRow();

        tr.addView(temp);
        tr.addView(tempData);
        return tr;
    }
    private TableRow getDateRow(DailyData data)
    {
        TableRow tr = getNewTableRow();

        TextView date = new TextView(MainActivity.this);
        date.setText(data.getDate());
        date.setGravity(Gravity.CENTER);

        TableRow.LayoutParams params = (TableRow.LayoutParams) tr.getLayoutParams();
        params.span = 2; //amount of columns you will span
        date.setLayoutParams(params);

        tr.addView(date);
        return tr;
    }
    private TableRow getNewTableRow()
    {
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        return tr;
    }
    private List<DailyData> getDailyData()
    {
        List<DailyData> daily = new Select()
                .from(DailyData.class)
                .orderBy("time ASC")
                .execute();
        if(daily.size() > 0) {
            return daily;
        }
        else
        {
            return null;
        }
    }
    private Currently getCurrently()
    {
        List<Currently> currently = new Select()
                .from(Currently.class)
                .execute();
        if(currently.size() > 0) {
            return currently.get(0);
        }
        else
        {
            return null;
        }
    }
    private Forecast getForecast()
    {
        List<Forecast> forecast = new Select()
                .from(Forecast.class)
                .execute();
        if(forecast.size() > 0) {
            return forecast.get(0);
        }
        else
        {
            return null;
        }
    }
    private Address getLocationFromZip(String zip)
    {
        final Geocoder geocoder = new Geocoder(this);
        try {
            List<Address> addresses = geocoder.getFromLocationName(zip, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                return address;
            } else {
                // Display appropriate message when Geocoder services are not available
                Toast.makeText(this, getString(R.string.unableToGetAddress), Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            // handle exception
        }
        return null;
    }
    private void getTownFromZip()
    {
        final Geocoder geocoder = new Geocoder(this);
        final String zip = "90210";
        try {
            List<Address> addresses = geocoder.getFromLocationName(zip, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                // Use the address as needed
                String message = String.format("Latitude: %f, Longitude: %f",
                        address.getLatitude(), address.getLongitude());
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            } else {
                // Display appropriate message when Geocoder services are not available
                Toast.makeText(this, "Unable to geocode zipcode", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            // handle exception
        }
    }
    @Override
    public void onStart()
    {
        super.onStart();
        Countly.sharedInstance().onStart(this);
    }

    @Override
    public void onStop()
    {
        Countly.sharedInstance().onStop();
        super.onStop();
    }
    private void enableCrashlytics()
    {
        Countly.sharedInstance().init(this, "http://countly.xkalq.com", "8a9ac0b847a97590ff0104dea042ba5ebb42eb7a", null, DeviceId.Type.OPEN_UDID);
        //I know it should be HTTPS but for some reason Android won't accept my industry-standard certificate on that server, and I don't have the time to figure out why this week.... so I had to disable it for now. Hopefully if it crashes again I can see it better.
        Countly.sharedInstance().setViewTracking(true);
        Countly.sharedInstance().enableCrashReporting();
        Countly.sharedInstance().setLoggingEnabled(true);
    }
}
