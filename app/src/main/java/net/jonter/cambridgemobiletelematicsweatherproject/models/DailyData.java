package net.jonter.cambridgemobiletelematicsweatherproject.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Table(name = "DailyData")
public class DailyData extends Model
{
    // This means that the table "Books" has a column named "Author"
    @Expose
    @Column(name = "time")
    public String time;

    @Expose
    @Column(name = "summary")
    public String summary;

    @Expose
    @Column(name = "temperatureLow")
    public String temperatureLow;

    @Expose
    @Column(name = "temperatureHigh")
    public String temperatureHigh;

    @Expose
    @Column(name = "humidity")
    public String humidity;

    public String getAverageTemperature()
    {
        if(temperatureHigh != null && temperatureLow != null)
        {
            DecimalFormat df = new DecimalFormat("###.##");
            return df.format((Float.parseFloat(temperatureHigh) + Float.parseFloat(temperatureLow)) / 2);
        }
        else
        {
            return null;
        }
    }

    public String getDate()
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.parseLong(time)*1000);
        return cal.get(Calendar.MONTH)+1 + "/" + cal.get(Calendar.DAY_OF_MONTH);
    }

    public DailyData()
    {
        // You have to call super in each constructor to create the table.
        super();
    }
}
