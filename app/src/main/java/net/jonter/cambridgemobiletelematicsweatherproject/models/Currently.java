package net.jonter.cambridgemobiletelematicsweatherproject.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Currently")
public class Currently extends Model
{
    // This means that the table "Books" has a column named "Author"
    @Expose
    @Column(name = "time")
    public String time;

    @Expose
    @Column(name = "summary")
    public String summary;

    @Expose
    @Column(name = "temperature")
    public String temperature;

    @Expose
    @Column(name = "humidity")
    public String humidity;


    public Currently()
    {
        // You have to call super in each constructor to create the table.
        super();
    }
}
