package net.jonter.cambridgemobiletelematicsweatherproject.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Forecasts")
public class Forecast extends Model
{
    @Expose
    @Column(name = "latitude")
    public String latitude;

    @Expose
    @Column(name = "longitude")
    public String longitude;

    @Expose
    @Column(name = "timezone")
    public String timezone;

    @Expose
    public Currently currently;

    @Expose
    public Daily daily;

    // public List<Currently> pageList;

    public Forecast()
    {

    }
}
