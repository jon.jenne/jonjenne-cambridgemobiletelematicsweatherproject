package net.jonter.cambridgemobiletelematicsweatherproject.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.List;

@Table(name = "Daily")
public class Daily extends Model
{

    @Expose
    @Column(name = "summary")
    public String summary;

    @Expose
    @Column(name = "data")
    public List<DailyData> data;

    public Daily()
    {
        // You have to call super in each constructor to create the table.
        super();
    }
}
